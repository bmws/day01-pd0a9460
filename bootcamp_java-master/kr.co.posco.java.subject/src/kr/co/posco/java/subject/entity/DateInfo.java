package kr.co.posco.java.subject.entity;

import java.util.Date;

public class DateInfo {

	private Date regDate;
	private Date modifyDate;

	public DateInfo() {}

	public DateInfo(Date regDate, Date modifyDate) {
		super();
		this.regDate = regDate;
		this.modifyDate = modifyDate;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	@Override
	public String toString() {
		return "DateInfo [regDate=" + regDate + ", modifyDate=" + modifyDate + "]";
	}
}
