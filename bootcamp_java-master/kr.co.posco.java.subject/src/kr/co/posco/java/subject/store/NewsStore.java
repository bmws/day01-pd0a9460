package kr.co.posco.java.subject.store;

import java.util.Date;
import java.util.List;

import kr.co.posco.java.subject.entity.News;

public interface NewsStore {
	
	News retrieveById(String id);
	
	List<News> retrieveAll();
	
	List<News> retrieveByContentProvider(String contentProvider);
	
	List<News> retrieveByData(Date today);

}
