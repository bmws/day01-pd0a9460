package kr.co.posco.java.subject.entity;

public class Author {

	private String reporter;
	private String contentProvider;

	public Author() {}

	public Author(String reporter, String contentProvider) {
		super();
		this.reporter = reporter;
		this.contentProvider = contentProvider;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public String getContentProvider() {
		return contentProvider;
	}

	public void setContentProvider(String contentProvider) {
		this.contentProvider = contentProvider;
	}

	@Override
	public String toString() {
		return "Author [reporter=" + reporter + ", contentProvider=" + contentProvider + "]";
	}
}
