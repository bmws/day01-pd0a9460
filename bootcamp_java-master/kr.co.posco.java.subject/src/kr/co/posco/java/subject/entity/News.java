package kr.co.posco.java.subject.entity;

import java.util.List;

public class News {

	private String id;
	private String title;
	private String description;
	private Author author;
	private DateInfo dateInfo;
	private List<String> keywords;

	public News() {
	}

	public News(String id, String title, String description, Author author, DateInfo dateInfo, List<String> keywords) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.author = author;
		this.dateInfo = dateInfo;
		this.keywords = keywords;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public DateInfo getDateInfo() {
		return dateInfo;
	}

	public void setDateInfo(DateInfo dateInfo) {
		this.dateInfo = dateInfo;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", description=" + description + ", author=" + author
				+ ", dateInfo=" + dateInfo + ", keywords=" + keywords + "]";
	}
}
