package kr.co.posco.java.subject.store.file;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import kr.co.posco.java.subject.entity.News;
import kr.co.posco.java.subject.store.NewsStore;
import kr.co.posco.java.subject.store.file.repository.NewsFileRepository;

public class NewsFileStore implements NewsStore{

	
	private NewsFileRepository repo;
	
	public NewsFileStore() {
		
		this.repo = NewsFileRepository.getInstance();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<News> retrieveAll() {
		// TODO Auto-generated method stub
		return repo.getNews().values().stream().collect(Collectors.toList());
	}

	@Override
	public List<News> retrieveByContentProvider(String contentProvider) {
		// TODO Auto-generated method stub
		return repo.getNews().values()
				.stream()
				.filter(cust -> cust.getAuthor().getReporter().equals(contentProvider))
				.collect(Collectors.toList());
	}

	@Override
	public List<News> retrieveByData(Date today) {
		// TODO Auto-generated method stub
		return repo.getNews().values()
				.stream()
				.filter(cust -> cust.getDateInfo().getRegDate().equals(today))
				.collect(Collectors.toList());
	}

	@Override
	public News retrieveById(String id) {
		// TODO Auto-generated method stub
		return repo.getNews().get(id);
	}
	
	
	

}
