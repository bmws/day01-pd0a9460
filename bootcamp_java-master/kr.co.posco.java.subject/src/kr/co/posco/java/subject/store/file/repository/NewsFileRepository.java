package kr.co.posco.java.subject.store.file.repository;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import kr.co.posco.java.subject.entity.Author;
import kr.co.posco.java.subject.entity.DateInfo;
import kr.co.posco.java.subject.entity.News;

public class NewsFileRepository {

	private static final String FILE_NAME = "news.json";
	
	private static NewsFileRepository uniqueInstance;
	
	private Map<String, News> news;
	
	
	private NewsFileRepository() {
		
		readCustomerFromFile();
		
	}
	
	public static NewsFileRepository getInstance() {
		if(uniqueInstance == null) {
			uniqueInstance = new NewsFileRepository();
			
		}
		return uniqueInstance;
		
	}
	
	public Map<String, News> getNews(){
		return news;
	}
	
	private void readCustomerFromFile() {
		File customerFile = getFileReader.apply(FILE_NAME);
		
		JSONParser parser = new JSONParser();
		
		try (Reader reader = new FileReader(customerFile)){

			JSONArray jsonArray = (JSONArray)parser.parse(reader);
			
			news = (Map<String, News>)jsonArray.stream().collect(Collectors.toMap(getKey, getValue));
					
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private Function<String, File> getFileReader = fileName -> {
		ClassLoader classLoader = getClass().getClassLoader();
		return new File(classLoader.getResource(fileName).getFile());
	};
	
	
	private Function<JSONObject, String> getKey = news -> (String)((JSONObject)news).get("id");
	

	private Function<JSONObject, News> getValue = json -> {

		
		News news = new News();
		news.setId((String)json.get("id"));
		news.setTitle(((String)json.get("title")));
		news.setDescription((String)json.get("description"));
		
		JSONObject authorJson = (JSONObject)json.get("author");
		Author author = new Author(
				(String)authorJson.get("reporter"),
				(String)authorJson.get("contentProvider"));
		
		news.setAuthor(author);


		JSONArray keywords = (JSONArray)json.get("keyword");
		news.setKeywords(keywords);
		
		JSONObject dateInfo = (JSONObject)json.get("date");
		Date regDate = new Date((long)dateInfo.get("regDate"));
		Date modifyDate = new Date((long)dateInfo.get("modifyDate"));
		DateInfo info = new DateInfo(regDate, modifyDate);
			
		news.setDateInfo(info);
		
		return news;
	};
	
}
